var struct___remmina_plugin_vnc_event =
[
    [ "button_mask", "struct___remmina_plugin_vnc_event.html#a03809ddab0aea8b6d2796641d4ccda32", null ],
    [ "event_data", "struct___remmina_plugin_vnc_event.html#a3eb3f92397d52e5e8c8e778450d1dcd6", null ],
    [ "event_type", "struct___remmina_plugin_vnc_event.html#a0ffbaa26908fd25e653dfaf5ad8ccff0", null ],
    [ "key", "struct___remmina_plugin_vnc_event.html#a464a014fb0aef74b464587f351cdf49f", null ],
    [ "keyval", "struct___remmina_plugin_vnc_event.html#ae2a04bad3d386a453554a431f9e9ad87", null ],
    [ "pointer", "struct___remmina_plugin_vnc_event.html#ab96d7c0c53fbe95aa88610b319aad895", null ],
    [ "pressed", "struct___remmina_plugin_vnc_event.html#a264ea1e41624de1e4a0e9cad031f0ac8", null ],
    [ "text", "struct___remmina_plugin_vnc_event.html#a79891d24c0e6257efceeefb3c10d8c65", null ],
    [ "text", "struct___remmina_plugin_vnc_event.html#a17fd02bcca976213ff16edde4b754ec6", null ],
    [ "x", "struct___remmina_plugin_vnc_event.html#a66390bbb63e42d25f8fa8e0be7f7dee8", null ],
    [ "y", "struct___remmina_plugin_vnc_event.html#a35d3190915553df1cfc65fb951403c11", null ]
];