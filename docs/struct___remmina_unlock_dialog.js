var struct___remmina_unlock_dialog =
[
    [ "builder", "struct___remmina_unlock_dialog.html#adea2159828578aefa4d3833083b9de2b", null ],
    [ "button_unlock", "struct___remmina_unlock_dialog.html#a0a4303634a8342c96dd71e8d7d3e9e7d", null ],
    [ "button_unlock_cancel", "struct___remmina_unlock_dialog.html#af4d3370e923f377e8e0af96deabc5a44", null ],
    [ "dialog", "struct___remmina_unlock_dialog.html#a2612295677b0f040e55470018c26628e", null ],
    [ "entry_unlock", "struct___remmina_unlock_dialog.html#ae104a7396def58ceb51949e73a9b5109", null ],
    [ "retval", "struct___remmina_unlock_dialog.html#a100f356b1eb9f6574b349e7573408235", null ],
    [ "unlock_init", "struct___remmina_unlock_dialog.html#a3ffac9bfa0017cd0e142adbba489274b", null ]
];