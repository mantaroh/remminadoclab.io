var NAVTREE =
[
  [ "Remmina - The GTK+ Remote Desktop Client", "index.html", [
    [ "Changelog", "md__c_h_a_n_g_e_l_o_g.html", null ],
    [ "CONTRIBUTING", "md__c_o_n_t_r_i_b_u_t_i_n_g.html", null ],
    [ "Table of contents", "md__builds__remmina_remmina-ci__remmina_8wiki__sidebar.html", null ],
    [ "Compilation-guide-for-RHEL", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compilation-guide-for-_r_h_e_l.html", null ],
    [ "Compilation", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compilation.html", null ],
    [ "Quick and dirty guide for compiling remmina on Arch", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_arch-_linux.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 10", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_debian-10-_buster.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 9", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_debian-9-_stretch.html", null ],
    [ "Compiling Remmina on FreeBSD 11", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_free_b_s_d.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 14.04", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_ubuntu-14_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 16.04", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_ubuntu-16_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 18.04", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_ubuntu-18_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 18.04", "md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_ubuntu-20_804.html", null ],
    [ "Requirements", "md__builds__remmina_remmina-ci__remmina_8wiki__contribution__h_o_w_t_o-generate-the-changelog.html", null ],
    [ "Development-Notes", "md__builds__remmina_remmina-ci__remmina_8wiki__development__development-_notes.html", null ],
    [ "Multi monitor support", "md__builds__remmina_remmina-ci__remmina_8wiki__development_multi-monitor-support.html", null ],
    [ "*Welcome to the Remmina wiki*", "md__builds__remmina_remmina-ci__remmina_8wiki__home.html", null ],
    [ "How-to-translate-Remmina", "md__builds__remmina_remmina-ci__remmina_8wiki__localisation__how-to-translate-_remmina.html", null ],
    [ "Update-gettext-messages", "md__builds__remmina_remmina-ci__remmina_8wiki__localisation__update-gettext-messages.html", null ],
    [ "GTK-versions-on-various-distributions", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks__g_t_k-versions-on-various-distributions.html", null ],
    [ "Problems-with-Wayland", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks__problems-with-_wayland.html", null ],
    [ "Remmina-RDP-and-HiDPI-scaling", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks__remmina-_r_d_p-and-_hi_d_p_i-scaling.html", null ],
    [ "Remmina-VNC-to-Raspbian-Stretch", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks__remmina-_v_n_c-to-_raspbian-_stretch.html", null ],
    [ "Systray menu", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks__systray-menu.html", null ],
    [ "How to configure key mapping with the VNC plugin.", "md__builds__remmina_remmina-ci__remmina_8wiki__problems-and-tweaks_vnc-key-mapping-configuration.html", null ],
    [ "Testing under Ubuntu with the remmina-next-daily PPA", "md__builds__remmina_remmina-ci__remmina_8wiki__testing__testing-under-_ubuntu-with-the-remmina-next-daily-_p_p_a.html", null ],
    [ "Using colour schemes in Remmina SSH protocol plugin", "md__builds__remmina_remmina-ci__remmina_8wiki__usage__remmina-_s_s_h-_terminal-colour-schemes.html", null ],
    [ "Remmina Usage FAQ", "md__builds__remmina_remmina-ci__remmina_8wiki__usage__remmina-_usage-_f_a_q.html", null ],
    [ "'s-guide Introduction", "md__builds__remmina_remmina-ci__remmina_8wiki__usage__remmina-_user.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"nx__plugin_8h.html#aef3e25a6bbd288f0a417a1fa331e661cae5a1b599d5b1d5f2aa74e3a3e7a9085a",
"rcw_8c.html#af4bcd597461ffa30d2975cbc6b071ce4",
"rdp__plugin_8h.html#a8a5bd2872aa895f3a76fa35caf77982a",
"remmina__file_8h.html#a3fb992fced83e0f1c4e4e3c8d2206be5",
"remmina__icon_8h.html#ae477ccd85ab1d878d243aed7fa5e48ae",
"remmina__nslookup_8sh.html",
"remmina__protocol__widget_8c.html#ad6e2d00646e8268aa0e8bbe31b77db48",
"remmina__ssh_8h.html",
"remmina__utils_8h.html#a116e7cd4565386fe531d3a6d1a3d3730",
"struct___remmina_group_data.html#a0044a7fb68569135efeb5769229d7f09",
"struct___remmina_plugin_ssh_data.html#aacebb7f6d617f5a753db5ceb450de45e",
"struct___remmina_protocol_feature.html#aa7ac5ec9d95867734619583a2049e952",
"structmpchanger__params.html#a098952d194a183d4a505aa065797864b",
"structrs_sched_data.html#a1747344f64896feb08c863ec2cf2528e",
"xdmcp__plugin_8c.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';