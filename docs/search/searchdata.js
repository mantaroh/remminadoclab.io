var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxy",
  1: "_dlmoprs",
  2: "_cdefghiklmnprstuvwx",
  3: "_abcdefghiklmnorsuvw",
  4: "abcdefghiklmnopqrstuvwxy",
  5: "dlpr",
  6: "cprw",
  7: "cdefglnoprstuvwx",
  8: "cdghmpqrstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

