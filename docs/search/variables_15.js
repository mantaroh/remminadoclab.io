var searchData=
[
  ['w',['w',['../struct_remmina_message_panel_private.html#a7cbab11c19f590bf2ec253eb694b93f8',1,'RemminaMessagePanelPrivate::w()'],['../structon_main_thread__cb__data.html#a2819729ddf9ffa92927a4ef7d3a6e0f8',1,'onMainThread_cb_data::w()'],['../structregion.html#a3b8e9f37558f5a790945b9dc1cbcadb6',1,'region::w()']]],
  ['wallpaper_5fcheck',['wallpaper_check',['../struct___remmina_plugin_rdpset_grid.html#aedf291b1b5cf61eb25731605e42fc319',1,'_RemminaPluginRdpsetGrid']]],
  ['webview',['webview',['../struct___remmina_plugin_w_w_w_data.html#a32adefd1fb55dd7237b907923d2a3690',1,'_RemminaPluginWWWData']]],
  ['widget',['widget',['../structon_main_thread__cb__data.html#af0d50114cec8cfdd7692a39c1fdba783',1,'onMainThread_cb_data']]],
  ['width',['width',['../struct___remmina_plugin_screenshot_data.html#ab5d115d8f9fc675f2419d38cdd424ce6',1,'_RemminaPluginScreenshotData::width()'],['../struct___remmina_protocol_widget_priv.html#a5cbea00dcc608759fcf66186e6afde93',1,'_RemminaProtocolWidgetPriv::width()'],['../structremmina__plugin__rdp__event.html#ac8972b1def0a956b7c36534da0e56b3b',1,'remmina_plugin_rdp_event::width()'],['../structremmina__plugin__rdp__ui__object.html#a37ad07cb6de3640c693fd1871615aa4f',1,'remmina_plugin_rdp_ui_object::width()'],['../structon_main_thread__cb__data.html#a8bf0f89b3c961ac28ccae9a9e1060db4',1,'onMainThread_cb_data::width()']]],
  ['window',['window',['../struct___remmina_connection_window.html#a587c1131f16856f8b114d0a51d261d0d',1,'_RemminaConnectionWindow::window()'],['../struct___remmina_chat_window.html#a60ccf6b47dcaf89b022eb6c2f71dcd29',1,'_RemminaChatWindow::window()'],['../struct___remmina_log_window.html#ae287dadd2c2a5bf704d7c494bcc74184',1,'_RemminaLogWindow::window()'],['../struct___remmina_main.html#a82aa6f22340fc4f3df185aba7b01afc2',1,'_RemminaMain::window()']]],
  ['window_5fid',['window_id',['../struct___remmina_plugin_nx_data.html#aec544598d9e3b88543a8f2f4d042a4fc',1,'_RemminaPluginNxData']]],
  ['windowdrag_5fcheck',['windowdrag_check',['../struct___remmina_plugin_rdpset_grid.html#a04a3bee67aa51808a69ded75e643e4c0',1,'_RemminaPluginRdpsetGrid']]]
];
