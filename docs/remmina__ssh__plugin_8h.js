var remmina__ssh__plugin_8h =
[
    [ "_RemminaProtocolSettingOpt", "struct___remmina_protocol_setting_opt.html", "struct___remmina_protocol_setting_opt" ],
    [ "RemminaProtocolSettingOpt", "remmina__ssh__plugin_8h.html#ace3febdfaa8e21a4c6a6108b4c54b5ec", null ],
    [ "remmina_plugin_ssh_vte_copy_clipboard", "remmina__ssh__plugin_8h.html#acb6514abadc6fcf387ef91e3c36bb146", null ],
    [ "remmina_plugin_ssh_vte_paste_clipboard", "remmina__ssh__plugin_8h.html#adf8c8f3dd7fa5412d8c580668cc36146", null ],
    [ "remmina_plugin_ssh_vte_select_all", "remmina__ssh__plugin_8h.html#a3290489a1ff9e51fbc013424fbfc8505", null ],
    [ "remmina_plugin_ssh_vte_terminal_set_encoding_and_pty", "remmina__ssh__plugin_8h.html#adc8e3776e67abca9ab913bc33172e4ce", null ],
    [ "remmina_ssh_plugin_popup_menu", "remmina__ssh__plugin_8h.html#a3e8e08f2a9b05530e85962f9b8c556e9", null ],
    [ "remmina_ssh_plugin_register", "remmina__ssh__plugin_8h.html#af9f9487a10fb1a9c4b494ce3657359b9", null ]
];