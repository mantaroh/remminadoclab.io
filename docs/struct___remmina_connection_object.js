var struct___remmina_connection_object =
[
    [ "aspectframe", "struct___remmina_connection_object.html#a45652d9f81875c51991ddf6942d7b2ae", null ],
    [ "cnnwin", "struct___remmina_connection_object.html#a871e61a40fb0acd54b10191a3942d328", null ],
    [ "connected", "struct___remmina_connection_object.html#aed1d667ed3085ee229c0dfdcf715084d", null ],
    [ "deferred_open_size_allocate_handler", "struct___remmina_connection_object.html#a09f89d6c0e4e52905aba1dd093e5c7f2", null ],
    [ "dynres_unlocked", "struct___remmina_connection_object.html#a71136a01b1cc4d14e0047b32ec4d8a4a", null ],
    [ "plugin_can_scale", "struct___remmina_connection_object.html#a1f3920f6710917d6944491ae1d7b1250", null ],
    [ "proto", "struct___remmina_connection_object.html#aea86c673d5c1600844e0d1642c856995", null ],
    [ "remmina_file", "struct___remmina_connection_object.html#a431b19fa993d0810e88cc973a307303c", null ],
    [ "scrolled_container", "struct___remmina_connection_object.html#aef266e6ae11db020512b3dec8f689d30", null ],
    [ "viewport", "struct___remmina_connection_object.html#a016a7b65f9db00458f3fc000c7e3a89d", null ]
];