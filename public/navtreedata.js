/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Remmina - The GTK+ Remote Desktop Client", "index.html", [
    [ "Changelog", "md__c_h_a_n_g_e_l_o_g.html", null ],
    [ "CONTRIBUTING", "md__c_o_n_t_r_i_b_u_t_i_n_g.html", null ],
    [ "Table of contents", "md__data_home_antenore_remmina_devel__remmina_8wiki__sidebar.html", null ],
    [ "Compilation-guide-for-RHEL", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compilation-guide-for-_r_h_e_l.html", null ],
    [ "Compilation", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compilation.html", null ],
    [ "Quick and dirty guide for compiling remmina on Arch", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_arch-_linux.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 10", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_debian-10-_buster.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 9", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_debian-9-_stretch.html", null ],
    [ "Compiling Remmina on FreeBSD 11", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_free_b_s_d.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 14.04", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_ubuntu-14_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 16.04", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_ubuntu-16_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 18.04", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_ubuntu-18_804.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 18.04", "md__data_home_antenore_remmina_devel__remmina_8wiki__compilation__compile-on-_ubuntu-20_804.html", null ],
    [ "Requirements", "md__data_home_antenore_remmina_devel__remmina_8wiki__contribution__h_o_w_t_o-generate-the-changelog.html", null ],
    [ "Development-Notes", "md__data_home_antenore_remmina_devel__remmina_8wiki__development__development-_notes.html", null ],
    [ "Multi monitor support", "md__data_home_antenore_remmina_devel__remmina_8wiki__development_multi-monitor-support.html", null ],
    [ "<em>Welcome to the Remmina wiki</em>", "md__data_home_antenore_remmina_devel__remmina_8wiki__home.html", null ],
    [ "How-to-translate-Remmina", "md__data_home_antenore_remmina_devel__remmina_8wiki__localisation__how-to-translate-_remmina.html", null ],
    [ "Update-gettext-messages", "md__data_home_antenore_remmina_devel__remmina_8wiki__localisation__update-gettext-messages.html", null ],
    [ "GTK-versions-on-various-distributions", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks__g_t_k-versions-on-various-distributions.html", null ],
    [ "Problems-with-Wayland", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks__problems-with-_wayland.html", null ],
    [ "Remmina-RDP-and-HiDPI-scaling", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks__remmina-_r_d_p-and-_hi_d_p_i-scaling.html", null ],
    [ "Remmina-VNC-to-Raspbian-Stretch", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks__remmina-_v_n_c-to-_raspbian-_stretch.html", null ],
    [ "Systray menu", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks__systray-menu.html", null ],
    [ "How to configure key mapping with the VNC plugin.", "md__data_home_antenore_remmina_devel__remmina_8wiki__problems-and-tweaks_vnc-key-mapping-configuration.html", null ],
    [ "Testing under Ubuntu with the remmina-next-daily PPA", "md__data_home_antenore_remmina_devel__remmina_8wiki__testing__testing-under-_ubuntu-with-the-remmina-next-daily-_p_p_a.html", null ],
    [ "Using colour schemes in Remmina SSH protocol plugin", "md__data_home_antenore_remmina_devel__remmina_8wiki__usage__remmina-_s_s_h-_terminal-colour-schemes.html", null ],
    [ "Remmina Usage FAQ", "md__data_home_antenore_remmina_devel__remmina_8wiki__usage__remmina-_usage-_f_a_q.html", null ],
    [ "'s-guide Introduction", "md__data_home_antenore_remmina_devel__remmina_8wiki__usage__remmina-_user.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"nx__plugin_8h.html#aef3e25a6bbd288f0a417a1fa331e661ca8e33d0950b052ef31232a134cf94a50e",
"rcw_8c.html#ae747a0b62a922df1de61c62092757b19",
"rdp__plugin_8h.html#a70cc17b1d3603d036f962a0ae8496b35ad2dd5f6251de0ac7afe2f962c829aa48",
"remmina__file_8h.html#a3be28ff7f5a9b44dc47f92b883a85ac5",
"remmina__icon_8h.html#a5b1bc5b1130b31ea9c9cb7164fb82f1d",
"remmina__mpchange_8h.html#a8627f0b2ac822c2f57a4eddc843a8fed",
"remmina__protocol__widget_8c.html#acf75fd4bac28a3d53da064b2905f23c1",
"remmina__ssh_8c.html#ad6f3f5c196175412049af0c440414e65",
"remmina__utils_8h.html",
"struct___remmina_file_plugin.html#af1bc6a2734ca8060af2943bf0dfca8d7",
"struct___remmina_plugin_ssh_data.html#a9e34d029c0a2b22affd19ae005e541ae",
"struct___remmina_protocol_feature.html#a1826baafd2d7b5dc8a009ef4fcad77e9",
"structlsb__distro__info.html#a7aba92d963bcaed1a5dda7b7be4fa6d7",
"structrf__pointer.html#a6e960686357ad03c023712bdfcef1cf0",
"www__utils_8h.html#aedac74feb0f847081efc43429d5b51cc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';