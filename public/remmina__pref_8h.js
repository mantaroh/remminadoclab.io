var remmina__pref_8h =
[
    [ "_RemminaColorPref", "struct___remmina_color_pref.html", "struct___remmina_color_pref" ],
    [ "_RemminaPref", "struct___remmina_pref.html", "struct___remmina_pref" ],
    [ "RemminaColorPref", "remmina__pref_8h.html#a058ab9dfe6100245f7c578c2e41d5970", null ],
    [ "RemminaPref", "remmina__pref_8h.html#a4e34cf29f6a4bff2d79cf621b5370c3b", null ],
    [ "REMMINA_VIEW_FILE_LIST", "remmina__pref_8h.html#aabfcbcb5ac86a1edac4035264bc7d2b8ae2bd62151481c5fa5823aa2d66fb67d7", null ],
    [ "REMMINA_VIEW_FILE_TREE", "remmina__pref_8h.html#aabfcbcb5ac86a1edac4035264bc7d2b8a46530ebfd659963a54b7dae5702af1df", null ],
    [ "REMMINA_ACTION_CONNECT", "remmina__pref_8h.html#a5d76b81b0ad4c19007a781d4edb8181fad14d19fdcc60e744406c5f80c1bb840a", null ],
    [ "REMMINA_ACTION_EDIT", "remmina__pref_8h.html#a5d76b81b0ad4c19007a781d4edb8181fa84fe14d6d19ab0479931a6886178f2e3", null ],
    [ "UNDEFINED_MODE", "remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034a86e471283504c1e470e4d0e7b8b5842f", null ],
    [ "SCROLLED_WINDOW_MODE", "remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034af963e665cf74d483ccc74e28531a480a", null ],
    [ "FULLSCREEN_MODE", "remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034a322281fd8bd9d95fb410b43ac9d496a3", null ],
    [ "SCROLLED_FULLSCREEN_MODE", "remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034a86e41f162b3b7d5bf1df78241ca1073e", null ],
    [ "VIEWPORT_FULLSCREEN_MODE", "remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034a61339886524adf33eaf7b5df6dd57955", null ],
    [ "FLOATING_TOOLBAR_PLACEMENT_TOP", "remmina__pref_8h.html#a0ed680fdb405e7195d9f14032851eebba9cdc370e02904c1f50681b7fe8bd1796", null ],
    [ "FLOATING_TOOLBAR_PLACEMENT_BOTTOM", "remmina__pref_8h.html#a0ed680fdb405e7195d9f14032851eebba7ed1345b9857b7029a8e0c61918dd95d", null ],
    [ "TOOLBAR_PLACEMENT_TOP", "remmina__pref_8h.html#a0944a4353780132eeab7b06e3e42291da069738542d6980f9594299ab57221a7f", null ],
    [ "TOOLBAR_PLACEMENT_RIGHT", "remmina__pref_8h.html#a0944a4353780132eeab7b06e3e42291dadb8aecbb1eda207dd5e18e665053ac27", null ],
    [ "TOOLBAR_PLACEMENT_BOTTOM", "remmina__pref_8h.html#a0944a4353780132eeab7b06e3e42291da95463ce23d03eb31b2bb61c29dab0076", null ],
    [ "TOOLBAR_PLACEMENT_LEFT", "remmina__pref_8h.html#a0944a4353780132eeab7b06e3e42291da4f58be8cee9e890445909866d5b11eaf", null ],
    [ "REMMINA_TAB_BY_GROUP", "remmina__pref_8h.html#a4790f45dcc812c1b00184a2edccdddf5a18b9b9f60fb377c589ff11b5388fee33", null ],
    [ "REMMINA_TAB_BY_PROTOCOL", "remmina__pref_8h.html#a4790f45dcc812c1b00184a2edccdddf5aedb9eb53656e5315bf173ec9d0f5b558", null ],
    [ "REMMINA_TAB_ALL", "remmina__pref_8h.html#a4790f45dcc812c1b00184a2edccdddf5afb68a1eeda5303774bbc922ef8d364e9", null ],
    [ "REMMINA_TAB_NONE", "remmina__pref_8h.html#a4790f45dcc812c1b00184a2edccdddf5adc41dc23d32d8955a469953bee975631", null ],
    [ "FLOATING_TOOLBAR_VISIBILITY_PEEKING", "remmina__pref_8h.html#a4caf8d8f829279fba122163d961608a4a3e85ac0b56e926949bc162505d199a2e", null ],
    [ "FLOATING_TOOLBAR_VISIBILITY_INVISIBLE", "remmina__pref_8h.html#a4caf8d8f829279fba122163d961608a4af179642fefe2ae4787e2d593519010dc", null ],
    [ "FLOATING_TOOLBAR_VISIBILITY_DISABLE", "remmina__pref_8h.html#a4caf8d8f829279fba122163d961608a4ad947c85d5ea917958d90298dab033b2c", null ],
    [ "remmina_pref_add_recent", "remmina__pref_8h.html#ab9e75c6bf39aa93a87d421fc3d742d60", null ],
    [ "remmina_pref_clear_recent", "remmina__pref_8h.html#ac3b15f811daf0aaf8bacd67f1b47aa14", null ],
    [ "remmina_pref_file_load_colors", "remmina__pref_8h.html#ad6424eb32c9549eb3e26f167dc58c218", null ],
    [ "remmina_pref_get_boolean", "remmina__pref_8h.html#a9a8d02227eb703bccc3f9296a5d89d6f", null ],
    [ "remmina_pref_get_recent", "remmina__pref_8h.html#a0208d5dc8197423da67e5967aafb7a05", null ],
    [ "remmina_pref_get_scale_quality", "remmina__pref_8h.html#a37cd589f00c94cb28eabb1bce4646d0b", null ],
    [ "remmina_pref_get_ssh_loglevel", "remmina__pref_8h.html#a12e52ee4ab89912280605e36ee069267", null ],
    [ "remmina_pref_get_ssh_parseconfig", "remmina__pref_8h.html#a98b783e4c5c1dae695fe554f52f94a23", null ],
    [ "remmina_pref_get_ssh_tcp_keepcnt", "remmina__pref_8h.html#a781431450dfcf639c8dd96701be3795b", null ],
    [ "remmina_pref_get_ssh_tcp_keepidle", "remmina__pref_8h.html#a5ed143a13f2a3393e44fd5b2c4fee4be", null ],
    [ "remmina_pref_get_ssh_tcp_keepintvl", "remmina__pref_8h.html#ad7040b17df053835bfb21eb24ae10414", null ],
    [ "remmina_pref_get_ssh_tcp_usrtimeout", "remmina__pref_8h.html#a1a695b64e75d6f49de220dc777e57714", null ],
    [ "remmina_pref_get_sshtunnel_port", "remmina__pref_8h.html#a6982152fd0492586819ce1760a853e99", null ],
    [ "remmina_pref_get_value", "remmina__pref_8h.html#a6bcbf4cc3a58ed4ee9f087b4270d8fd6", null ],
    [ "remmina_pref_init", "remmina__pref_8h.html#a400f7319fd3996a13399ef72bcf0a2fd", null ],
    [ "remmina_pref_is_rw", "remmina__pref_8h.html#a0dd25f5b3a055b936a5fa327bc71f19c", null ],
    [ "remmina_pref_keymap_get_keyval", "remmina__pref_8h.html#aaac087c000b0863a296f2739f6344af8", null ],
    [ "remmina_pref_keymap_groups", "remmina__pref_8h.html#a745bbef1d48a7462f8db997821742395", null ],
    [ "remmina_pref_save", "remmina__pref_8h.html#ae17aaf37449083a4322b6748c8f3a39f", null ],
    [ "remmina_pref_set_value", "remmina__pref_8h.html#a4fbb2aff07d1fef416352d8b80d79e02", null ],
    [ "default_resolutions", "remmina__pref_8h.html#a316f4290c083d28bd3492ee979278290", null ],
    [ "remmina_colors_file", "remmina__pref_8h.html#a94d3cf980275b6e9b0c701972f8b1cce", null ],
    [ "remmina_pref", "remmina__pref_8h.html#a29701ae152ba15f6d8921f395174d2df", null ],
    [ "remmina_pref_file", "remmina__pref_8h.html#af657fd4825d16d8d003a8b42fbd0c715", null ]
];