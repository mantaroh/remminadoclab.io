var struct___remmina_file_plugin =
[
    [ "description", "struct___remmina_file_plugin.html#a36c131a8e8c0180950faa80f497f23c1", null ],
    [ "domain", "struct___remmina_file_plugin.html#a1de86485b3c4db138b0fcd264884483f", null ],
    [ "export_func", "struct___remmina_file_plugin.html#ab721bcec1729b6681a71c2fb196878f1", null ],
    [ "export_hints", "struct___remmina_file_plugin.html#ac4f2b0118490cede0577aaee9f2e0b5f", null ],
    [ "export_test_func", "struct___remmina_file_plugin.html#aafae42901b54bcb08f0cb24451836b03", null ],
    [ "import_func", "struct___remmina_file_plugin.html#af1bc6a2734ca8060af2943bf0dfca8d7", null ],
    [ "import_test_func", "struct___remmina_file_plugin.html#ac00efb2a471517b35fbd386c29ee18f4", null ],
    [ "name", "struct___remmina_file_plugin.html#a8ef191449f269aec279e9097cadb7ee6", null ],
    [ "type", "struct___remmina_file_plugin.html#a3ff85e56bf902817630aafea67e3e494", null ],
    [ "version", "struct___remmina_file_plugin.html#a57bf1f4e71c86165cbac2563309e35b8", null ]
];