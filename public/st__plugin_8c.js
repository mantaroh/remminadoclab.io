var st__plugin_8c =
[
    [ "_RemminaPluginData", "struct___remmina_plugin_data.html", "struct___remmina_plugin_data" ],
    [ "RemminaPluginData", "st__plugin_8c.html#a1d05db3e17981752867852820f99b1c4", null ],
    [ "remmina_plugin_entry", "st__plugin_8c.html#a56e5020de11446dbc23f3480e313f208", null ],
    [ "remmina_plugin_st_close_connection", "st__plugin_8c.html#acb647e0f3bf350dce499eeff97dce73c", null ],
    [ "remmina_plugin_st_init", "st__plugin_8c.html#a32c62a761eb95bc5c85c433ea4c53f5d", null ],
    [ "remmina_plugin_st_on_plug_added", "st__plugin_8c.html#ad71e864fed6cf20a01a0dc7c38e98fe7", null ],
    [ "remmina_plugin_st_on_plug_removed", "st__plugin_8c.html#ae829911e1c265665933ab5318f462466", null ],
    [ "remmina_plugin_st_open_connection", "st__plugin_8c.html#af8288a0569d8de6fe991fb5292544430", null ],
    [ "remmina_st_query_feature", "st__plugin_8c.html#a270fb394bd933b0a3fcf53aa55f08b01", null ],
    [ "remmina_plugin", "st__plugin_8c.html#a3b53d8bbfcf5bdf5564c8804d211cf99", null ],
    [ "remmina_plugin_service", "st__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7", null ],
    [ "remmina_plugin_st_advanced_settings", "st__plugin_8c.html#aa3b746a1bdc4f315310a9c51394d5c46", null ],
    [ "remmina_plugin_st_basic_settings", "st__plugin_8c.html#a69a3fe4c39d316ac75da7edf98d73cf1", null ],
    [ "remmina_st_features", "st__plugin_8c.html#ae13f8b88aeee98040a215ecec433a8f4", null ],
    [ "term_list", "st__plugin_8c.html#a0d25ec88a2123202a652978635d8430a", null ]
];