var kwallet__plugin__main_8c =
[
    [ "build_kwallet_key", "kwallet__plugin__main_8c.html#a864de43408740eb3b6ef58c820773e2a", null ],
    [ "remmina_plugin_entry", "kwallet__plugin__main_8c.html#a56e5020de11446dbc23f3480e313f208", null ],
    [ "remmina_plugin_kwallet_delete_password", "kwallet__plugin__main_8c.html#aebc944531076dba0af8a531abfcd01f6", null ],
    [ "remmina_plugin_kwallet_get_password", "kwallet__plugin__main_8c.html#ac43da8da793eaf8a37af70c7fe96e2c8", null ],
    [ "remmina_plugin_kwallet_init", "kwallet__plugin__main_8c.html#a6ef517573fb85cfca2843c7f8358d14d", null ],
    [ "remmina_plugin_kwallet_is_service_available", "kwallet__plugin__main_8c.html#ac1ca29a8219b7e34330c6a10f4ea3d76", null ],
    [ "remmina_plugin_kwallet_store_password", "kwallet__plugin__main_8c.html#af53acf42b7887610820f47a3323aa650", null ],
    [ "remmina_plugin_kwallet", "kwallet__plugin__main_8c.html#af31524d2a9ee0747edc915947283b469", null ],
    [ "remmina_plugin_service", "kwallet__plugin__main_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7", null ]
];