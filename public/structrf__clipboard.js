var structrf__clipboard =
[
    [ "SCDW_NONE", "structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438a4b0fb5c70fe8ee6bbd80cb085f8571a1", null ],
    [ "SCDW_BUSY_WAIT", "structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438aac2dccc58111570a94b8ec7475a8110e", null ],
    [ "SCDW_ABORTING", "structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438af9766a14efb1de3fd44ed4fb71622e5b", null ],
    [ "clipboard_handler", "structrf__clipboard.html#a52ca364a1902d7f4095234923feed724", null ],
    [ "context", "structrf__clipboard.html#a773214c0bb4f351a609a085f786304aa", null ],
    [ "format", "structrf__clipboard.html#ad22c0f3cd325db2746e9ca42cca0d65b", null ],
    [ "requestedFormatId", "structrf__clipboard.html#a66caa2e83dfdc4655df71d81502f93e3", null ],
    [ "rfi", "structrf__clipboard.html#a268b5336ae92d8d2f54c5ee441325d68", null ],
    [ "srv_clip_data_wait", "structrf__clipboard.html#ae2405c3546b91c1ae200b14ef0e68026", null ],
    [ "srv_data", "structrf__clipboard.html#ae14721fd73459cc2be441c96f5a96be7", null ],
    [ "system", "structrf__clipboard.html#ad7541d40ba2e4463c36ffab12cfe3adf", null ],
    [ "transfer_clip_cond", "structrf__clipboard.html#acf5bf9da1a18e21409c4b10368f8dfd7", null ],
    [ "transfer_clip_mutex", "structrf__clipboard.html#a5c6be1637ed9e2ce72ffde2ed5400205", null ]
];